package com.xjt.excel.mapper;

import com.xjt.excel.entity.PubImpExcelModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xjt.excel.entity.PubImpExcelModelcol;

import java.util.List;

/**
 * <p>
 * 批量导入配置信息表 Mapper 接口
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface PubImpExcelModelMapper extends BaseMapper<PubImpExcelModel> {


    void createTable(String tableName, List<PubImpExcelModelcol> list);

    Integer saveData(String tableName,List<List<String>> list);
}
