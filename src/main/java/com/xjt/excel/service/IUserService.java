package com.xjt.excel.service;

import com.xjt.excel.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface IUserService extends IService<User> {

}
