package com.xjt.excel.service;

import com.xjt.excel.entity.PubImpExcelValidLog;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 批量导入数据校验日志信息表 服务类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-18
 */
public interface IPubImpExcelValidLogService extends IService<PubImpExcelValidLog> {

}
