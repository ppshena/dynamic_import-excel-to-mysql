package com.xjt.excel.service;

import com.xjt.excel.entity.PubImpExcelValidRule;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 批量导入数据校验规则信息表 服务类
 * </p>
 *
 * @author xujiangtao
 * @since 2021-05-19
 */
public interface IPubImpExcelValidRuleService extends IService<PubImpExcelValidRule> {

}
