SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- 初始化sql
-- ----------------------------
-- Table structure for pub_imp_excel_jd_log
-- ----------------------------
DROP TABLE IF EXISTS `pub_imp_excel_jd_log`;
CREATE TABLE `pub_imp_excel_jd_log`  (
  `id` bigint(18) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `menu_id` bigint(18) NULL DEFAULT NULL COMMENT '操作菜单ID',
  `pc_num` varchar(32) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '批次UUID',
  `jd_state` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '进度名称,大批量异步上传，每完成一个阶段，更新此名称，便于用户了解导入进度',
  `state` int(11) NULL DEFAULT NULL COMMENT '有效性状态，1表示有效，9表示已撤销',
  `start_time` datetime NULL DEFAULT NULL COMMENT '导入开始时间',
  `end_time` datetime NULL DEFAULT NULL COMMENT '导入截止时间',
  `oper_uid` bigint(18) NULL DEFAULT NULL COMMENT '操作用户',
  `source_file_name` varchar(200) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '上传数据表文件名称，便于用户识别',
  `problemnum` int(11) NULL DEFAULT NULL COMMENT '校验不通过记录数',
  `souce_file_path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '导入文件上传后在服务器上的存储路径（不展示，用于开发人员检索核对）',
  `tmp_table_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '生成临时表名称',
  `md5code` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '文件的MD5码（判断文件重复，则不需要导入）',
  `total_row` int(11) NULL DEFAULT NULL COMMENT '导入记录总数（总行数）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 151 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '批量导入日志信息表（提供导入日志查询功能）' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pub_imp_excel_model
-- ----------------------------
DROP TABLE IF EXISTS `pub_imp_excel_model`;
CREATE TABLE `pub_imp_excel_model`  (
  `id` bigint(13) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `oper_uid` bigint(13) NULL DEFAULT NULL COMMENT '操作人员ID，通用型操作人员id为0',
  `menu_id` bigint(13) NULL DEFAULT NULL COMMENT '菜单ID,导入按钮菜单ID，一表多sheet情况menu_id相同',
  `state` int(3) NULL DEFAULT 1 COMMENT '状态，1表示有效，9表示无效',
  `sheet_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'sheet表名,空表示循环读取（多sheet循环读取）',
  `model_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导入模板名称，便于识别',
  `ind` int(3) NULL DEFAULT NULL COMMENT '显示顺序号',
  `service_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '导入实现类名称',
  `fixed_col` int(1) NULL DEFAULT 1 COMMENT '是否固定列（1表示系统预置列字段对应关系，0表示由用户选择列对应关系）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '批量导入配置信息表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pub_imp_excel_modelcol
-- ----------------------------
DROP TABLE IF EXISTS `pub_imp_excel_modelcol`;
CREATE TABLE `pub_imp_excel_modelcol`  (
  `id` bigint(13) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `model_id` bigint(13) NULL DEFAULT NULL COMMENT '菜单设置ID,与pub_imp_excel_model相关联',
  `col_code` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '列字段编码（数据库字段）',
  `excel_column` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT 'Excel列名（临时表字段名）,不指定列名，',
  `state` int(3) NULL DEFAULT 1 COMMENT '状态，1表示有效，9表示无效',
  `validate_zz` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据验证正则表达式（），由valid数学表达式组合生成。复合涉及公式部分由AviatorEvaluator解析，参照导出ExcelWriteUtil.convertDataToRow表达式。',
  `validate_tip` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '1' COMMENT '数据验证不通过提示信息。',
  `is_req` int(1) NOT NULL DEFAULT 0 COMMENT '是否必填项。1表示必填，0表示非必填。必填项为空或为null,则提示此项为必填项。',
  `ind` int(10) NULL DEFAULT NULL COMMENT '显示顺序号',
  `col_name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '正式库字段名',
  `field_type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '字段类型及长度（用于创建临时表），如varchar(100)',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 36 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '批量导入数据字段对应配置信息表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pub_imp_excel_valid_log
-- ----------------------------
DROP TABLE IF EXISTS `pub_imp_excel_valid_log`;
CREATE TABLE `pub_imp_excel_valid_log`  (
  `id` bigint(18) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `col_id` bigint(18) NULL DEFAULT NULL COMMENT '列ID',
  `valid_tip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验提示语',
  `jdlog_id` bigint(18) NULL DEFAULT NULL COMMENT '导入ID,与pub_imp_excel_jd_log相关联',
  `row_id` bigint(13) NULL DEFAULT NULL COMMENT '行序号',
  `sheet_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT 'sheet名称',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 15436 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '批量导入数据校验日志信息表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for pub_imp_excel_valid_rule
-- ----------------------------
DROP TABLE IF EXISTS `pub_imp_excel_valid_rule`;
CREATE TABLE `pub_imp_excel_valid_rule`  (
  `id` bigint(18) NOT NULL AUTO_INCREMENT COMMENT '主键ID',
  `state` int(1) NULL DEFAULT NULL COMMENT '状态，1表示有效，9表示无效',
  `valid_expression` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验正则表达式',
  `valid_tip` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验提示信息',
  `valid_code` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验规则代码',
  `remark` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL COMMENT '校验规则名称（便于识别作用）',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci COMMENT = '批量导入数据校验规则信息表' ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;


